from flask import Flask, request, url_for, render_template
import sys,os

app = Flask(__name__)

def get_srcript_path():
	curMod = sys.modules.get(__name__)
	if curMod  is not None and  hasattr(curMod, '__file__'):
		print(curMod.__file__)
		print(os.path.abspath(curMod.__file__))
		print(os.path.dirname(os.path.abspath(curMod.__file__)))


@app.route('/')
def hello_world():
	return render_template('base.html')

# url 
@app.route('/name')
def name():
	print(url_for('hello_world'))
	return "name what are you "

#http methods
@app.route('/login/<username>', methods=['GET', 'POST'])
def login(username):
	if request.method == 'GET':
		print(url_for('name'))
		return 'hello %s'% username
	else:
		return 'newbee are'

#static file 
@app.route('/file')
def staticFile():
	return url_for('static', filename='base.css')

if __name__ == '__main__':
	get_srcript_path()
	app.run()
