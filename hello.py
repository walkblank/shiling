from flask import Flask, render_template, url_for

app = Flask(__name__)

@app.route('/')
def hello_world():
    return render_template('hello.html')

@app.route('/user/<username>')
def name(username):
    return render_template('hello.html', user = username)

@app.route('/base')
def base():
    return render_template('temp.html')